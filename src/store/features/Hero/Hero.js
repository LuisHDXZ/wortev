import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  heroList: {}
}

export const heroListSlice = createSlice({
  name: 'heroList',
  initialState,
  reducers: {
    updateHeroList: (state, action) => {
      state.heroList = { ...action.payload }
    },
  },
})

export const { increment, decrement, updateHeroList } = heroListSlice.actions

export default heroListSlice.reducer