import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  showLoader: false
}

export const LoaderSlice = createSlice({
  name: 'loader',
  initialState,
  reducers: {
    changeLoader: (state, action) => {
      state.showLoader = action.payload
    },
  },
})

export const { increment, decrement, changeLoader } = LoaderSlice.actions

export default LoaderSlice.reducer