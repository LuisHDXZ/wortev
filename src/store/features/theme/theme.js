import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  mode: 'dark'
}

export const themeSlice = createSlice({
  name: 'theme',
  initialState,
  reducers: {
    updateTheme: (state, action) => {
      state.mode = state.mode === 'dark' ? 'ligth' : 'dark'
    },
  },
})

// Action creators are generated for each case reducer function
export const { increment, decrement, updateTheme } = themeSlice.actions

export default themeSlice.reducer