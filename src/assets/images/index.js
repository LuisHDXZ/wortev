import MarvelLogo from './png/Marvel_Logo.svg.png';
import SpiderMan from './png/spiderMan.png'
import SpiderManBack from './png/spidermanback.png'
import MarvelBg from './jpg/marvelBg.jpg'
import HulkLogo from './png/hulk.png'
import IronmanLogo from './png/ironman.png'
import ScarletLogo from './png/scarlet.png'
import SpidermanLogo from './png/spiderman-logo.png'
import ThorLogo from './png/thor.png'
import IronManBg from './jpg/iron-man.jpg'

const images = {
  MarvelLogo,
  SpiderMan,
  SpiderManBack,
  MarvelBg,
  IronManBg,
  HulkLogo,
  IronmanLogo,
  ScarletLogo,
  SpidermanLogo,
  ThorLogo
}

export { images }