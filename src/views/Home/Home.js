import React, { useEffect, useState } from 'react'
import { Container, Grid, Typography } from '@mui/material'
import { useStyles } from './HomeStyles';
import { images } from '../../assets/images';
import { useApi } from '../../hooks/useApi'
import { getItem, setItem } from '../../utils/persistentStorage';
import { CarouselMedia } from '../../components/Carousel';
import { aArregloDeArreglos } from './utils'

export const Home = () => {
  const classes = useStyles();
  const [imageBackground, setimageBackground] = useState('')
  const [comicList, setcomicList] = useState([])
  const comicsList = getItem('comics')
  const [_handleGetComics] = useApi({
    method: 'get',
    endpoint: '/comics'
  });

  const getComics = async () => {
    const response = await _handleGetComics({})
    if (response.code === 200) setItem('comics', response.data.results); setcomicList(response.data.results)
  }

  useEffect(() => {
    comicsList === null ? getComics() : setcomicList(comicsList)
  }, [])

  return (
    <div>
      <div className={classes.root}>
        <Container>
          <Grid>
            <Typography className={classes.title}>
              Marvel Heroes
            </Typography>
          </Grid>
          <Grid container spacing={3}>
            <Grid item md={7} style={{ height: '100%' }}>
              <Typography className={classes.info}>Marvel Entertainment, LLC es una compañía de medios estadounidense y una subsidiaria de The Walt Disney Company.  Fue fundada como Timely Publications en 1939 por el editor Martin Goodman, y desde 1961 ha estado publicando bajo el nombre Marvel Comics. <br />El crecimiento de Marvel se basa en el uso de personajes ficticios con superpoderes como Spider-Man, Iron Man, Hulk, Thor, Capitán América, Doctor Strange y Los Vengadores.</Typography>
            </Grid>
            <Grid item md={3}>
              <img src={images.SpiderManBack} alt='Spiderman' className={classes.image} />
            </Grid>
          </Grid>
        </Container>
      </div>
      <div
        style={{ backgroundImage: `url("${imageBackground}")` }}
        className={classes.comicContainer}
      >
        <div className={classes.effect}>
          <Container>
            <Grid container>
              <Grid style={{ marginTop: '5vh' }} item md={12}>
                <Typography className={classes.title}>Ultimos Comics</Typography>
              </Grid>
              <Grid item md={12} xs={12} style={{ minHeight: '70vh' }}>
                {comicList !== null &&
                  <CarouselMedia
                    items={aArregloDeArreglos(comicList)}
                    updateImage={(urlImage) => setimageBackground(urlImage)}
                  />
                }
              </Grid>
            </Grid>
          </Container>
        </div>
      </div>
    </div>
  )
}
