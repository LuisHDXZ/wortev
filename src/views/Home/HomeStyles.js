import { makeStyles } from '@mui/styles';
import { images } from '../../assets/images';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundImage: `url("${images.MarvelBg}")`,
    backgroundAttachment: 'fixed',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    minHeight: '100vh'
  },
  image: {
    height: '90vh',
    animation: '$move-down 8s linear infinite',
    marginTop: '-15vh',
    [theme.breakpoints.down('md')]: {
      marginTop: '0vh',
      height: '60vh',
    }
  },
  '@keyframes move-down': {
    '0%': {
      transform: 'translateY(0)',
    },
    '50%': {
      transform: 'translateY(-5%)',
    },
    '90%': {
      transform: 'translateY(10%)',
    },
    '100%': {
      transform: 'translateY(0)',
    },
  },
  info: {
    backdropFilter: 'blur(3px)',
    fontSize: '35px !important',
    padding: '1vh',
    [theme.breakpoints.down('md')]: {
      fontSize: '20px !important',
    }
  },
  title: {
    fontSize: '60px !important',
    textAlign: 'center',
    fontWeight: '800!important',
    backdropFilter: 'blur(3px)',
    [theme.breakpoints.down('md')]: {
      fontSize: '45px !important',
    }
  },
  comicContainer: {
    minHeight: '70vh !important',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    transition: 'all 1s ease-in-out ',
  },
  effect: {
    backdropFilter: 'blur(18px)',
    height: '100%',
    paddingBottom: '4vh'
  },
}));

export { useStyles };
