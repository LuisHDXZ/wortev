function aArregloDeArreglos(arreglo) {
  let arregloDeArreglos = [];
  let posicionActual = 0;
  while (posicionActual < arreglo.length) {
    let arregloAuxiliar = [];
    for (let i = 0; i < 3; i++) {
      if (arreglo[posicionActual] !== undefined) {
        arregloAuxiliar.push(arreglo[posicionActual]);
      }
      posicionActual++;
    }
    arregloDeArreglos.push(arregloAuxiliar);
  }
  return arregloDeArreglos;
}

export { aArregloDeArreglos }