import React, { useEffect, useState } from 'react'
import { Container, Grid, Typography } from '@mui/material'
import { getItem, setItem } from '../../utils/persistentStorage';
import { useApi } from '../../hooks/useApi'
import { ComicCard } from '../../components/ComicCard/ComicCard';
import { useStyles } from './ComicCardStyles';

export const Comics = () => {
  const [comicList, setcomicList] = useState([])
  const classes = useStyles();
  const comicsList = getItem('comics')
  const [_handleGetComics] = useApi({
    method: 'get',
    endpoint: '/comics'
  });
  const getComics = async () => {
    const response = await _handleGetComics({})
    if (response.code === 200) setItem('comics', response.data.results); setcomicList(response.data.results)
  }

  useEffect(() => {
    comicsList === null ? getComics() : setcomicList(comicsList)
  }, [])
  return (
    <div>
      <Container>
        <Grid container style={{ padding: '4vh 0vh' }}>
          <Grid item md={12} xs={12}>
            <Typography className={classes.title}>Comics</Typography>
          </Grid>
          {comicList.map(comic =>
            <Grid item md={4} sx={{ mb: 3, mt: 3 }}>
              <ComicCard
                heroName={comic.title}
                description={`$ ${comic.prices[0].price}`}
                thumbnail={comic.thumbnail}
              />
            </Grid>
          )}
        </Grid>
      </Container>
    </div>
  )
}
