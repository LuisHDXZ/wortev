import { makeStyles } from '@mui/styles';


const useStyles = makeStyles((theme) => ({
  mainContainer: {
    padding: '5vh'
  },
  title: {
    fontSize: '60px !important',
    textAlign: 'center'
  }
}));

export { useStyles };
