import React, { useEffect } from 'react'
import { HeroCard } from '../../components/HeroCard/HeroCard'
import { Grid, Typography } from '@mui/material'
import { useApi } from '../../hooks/useApi'
import { useDispatch, useSelector } from 'react-redux'
import { updateHeroList } from '../../store/features/Hero/Hero'
import { useStyles } from './HeroListStyles';

export const HeroList = () => {
  const selector = useSelector((state) => state.heroList.heroList);
  const dispatch = useDispatch()
  const classes = useStyles();

  const [_handleGetCharacters] = useApi({
    method: 'get',
    endpoint: '/characters'
  });

  const getCharacters = async () => {
    const response = await _handleGetCharacters({})
    dispatch(updateHeroList(response.data))
  }
  useEffect(() => {
    Object.keys(selector).length === 0 && getCharacters()
  }, [])

  return (
    <Grid className={classes.mainContainer}>
      <Grid container spacing={4}>
        <Grid item md={12} xs={12}>
          <Typography className={classes.title}>Personajes</Typography>
        </Grid>
        {Object.keys(selector).length !== 0 && selector.results.map(hero =>
          <Grid item md={3} xs={12} key={hero.name}>
            <HeroCard
              heroName={hero.name}
              description={hero.description}
              thumbnail={hero.thumbnail}
            />
          </Grid>
        )}
      </Grid>
    </Grid>
  )
}
