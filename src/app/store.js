import { configureStore } from '@reduxjs/toolkit'
import themeReducer from '../store/features/theme/theme'
import heroListReducer from '../store/features/Hero/Hero'
import loaderReducer from '../store/features/Loader/Loader'

export const store = configureStore({
  reducer: {
    theme: themeReducer,
    heroList: heroListReducer,
    loader: loaderReducer
  },
})