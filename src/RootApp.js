import React from 'react'
import { ThemeProvider } from '@mui/material/styles';
import { useSelector } from 'react-redux'
import { lightTheme, darkTheme } from './theme'
import {
  Routes,
  Route,
  useLocation
} from 'react-router-dom';
import { routes } from './Routes/routes'
import { CSSTransition, SwitchTransition } from "react-transition-group";


export const RootApp = () => {
  const selector = useSelector((state) => state.theme.mode);
  const location = useLocation()
  const { nodeRef } =
    routes.find((route) => route.path === location.pathname) ?? {}

  const mapRoute = (item) => {
    return (
      <SwitchTransition >
        <CSSTransition
          key={location.key}
          nodeRef={nodeRef}
          timeout={30000}
          classNames="page"
          unmountOnExit>
          <Routes>
            <Route path={item.path} exact={item.exact}
              element={
                <item.layout ref={nodeRef}>
                  <item.component />
                </item.layout>
              }
            />
          </Routes>
        </CSSTransition>
      </SwitchTransition>)
  }
  return (
    <ThemeProvider theme={selector === 'dark' ? darkTheme : lightTheme} >
      {routes.map(route => mapRoute(route))}
    </ThemeProvider >

  )
}
