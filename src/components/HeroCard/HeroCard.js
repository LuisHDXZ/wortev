import { Grid, Typography } from '@mui/material'
import React from 'react'
import { useStyles } from './HeroCardStyles';


export const HeroCard = ({ heroName, description, thumbnail }) => {
  const classes = useStyles();

  return (
    <div className={classes.cardImage}
      style={{ backgroundImage: `url("${thumbnail.path}.${thumbnail.extension}")` }}>
      <div className={classes.content}>
        <Grid>
          <Typography className={classes.title}>
            {heroName}
          </Typography>
          <Typography className={classes.description}>
            {description}
          </Typography>
        </Grid>
      </div>
    </div>
  )
}
