import { makeStyles } from '@mui/styles';


const useStyles = makeStyles((theme) => ({
  title: {
    fontSize: '30px !important',
    fontWeight: '800 !important',
    marginBottom: '1vh'
  },
  description: {
    fontSize: '20px !important',
    fontWeight: '800 !important',
    marginBottom: '1vh',
    textJustify: 'inter-word',
    width: '100%'
  },
  content: {
    width: '90%',
    top: '50%',
    height: '92%',
    backgroundColor: 'rgba(0,0,0,0.9)',
    borderRadius: '12px',
    color: 'white',
    fontSize: '1.5rem',
    padding: '20px',
    textAlign: 'center',
    opacity: 0,
    transition: 'all 0.5s ease',
    '&:hover': {
      opacity: 1,
    },
  },
  cardImage: {
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    width: '95%',
    height: '500px !important',
    border: '3px solid white',
    borderRadius: '12px',
    transition: '1s ease',
    '&:hover': {
      border: 'none',
    },
  }
}));

export { useStyles };
