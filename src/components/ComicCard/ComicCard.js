import { Grid, Typography } from '@mui/material'
import React from 'react'
import { useStyles } from './ComicCardStyles';
import PropTypes from 'prop-types';

export const ComicCard = ({ heroName, description, thumbnail, updateImage }) => {
  const classes = useStyles();
  return (
    <div className={classes.cardImage}
      style={{ backgroundImage: `url("${thumbnail.path}.${thumbnail.extension}")` }}
      onMouseEnter={() => updateImage(`${thumbnail.path}.${thumbnail.extension}`)}
      onMouseLeave={() => updateImage("")}
    >
      <div className={classes.content}>
        <Grid>
          <Typography className={classes.title}>
            {heroName}
          </Typography>
          <Typography className={classes.description}>
            {description}
          </Typography>
        </Grid>
      </div>
    </div>
  )
}

ComicCard.propTypes = {
  heroName: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  thumbnail: PropTypes.shape({ path: PropTypes.string, extension: PropTypes.string }).isRequired,
  updateImage: PropTypes.func
};
ComicCard.defaultProps = {
  updateImage: () => console.warn("no se envio la función")
}