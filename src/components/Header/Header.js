import { Grid, Toolbar, Box, AppBar } from '@mui/material'
import React from 'react'
import { images } from '../../assets/images'
import { useStyles } from './HeaderStyles';

export const Header = () => {
  const classes = useStyles();

  const menuItem = [
    { label: 'Home', path: '/' },
    { label: 'Personajes', path: '/characteres' },
    { label: 'Comics', path: '/Comics' },
  ]
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Grid container spacing={1}>
            <Grid item md={9}>
              <img src={images.MarvelLogo} alt='Marve-logo' style={{ height: '6vh' }} />
            </Grid>
            {menuItem.map(optionMenu =>
              <Grid item md={1} key={optionMenu.path} className={classes.centerContent}>
                <a href={optionMenu.path} className={classes.link}>{optionMenu.label}</a>
              </Grid>
            )}
          </Grid>
        </Toolbar>
      </AppBar>
    </Box>
  )
}
