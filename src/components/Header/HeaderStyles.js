import { makeStyles } from '@mui/styles';


const useStyles = makeStyles((theme) => ({
  link: {
    textDecoration: 'none',
    color: theme.palette.primary.main,
    transition: 'all 1s ease-in-out ',
    fontSize: '20px'
  },
  centerContent: {
    display: 'flex',
    textAlign: 'center',
    alignSelf: 'center',
    '& :hover': {
      color: 'red'
    }
  }
}));

export { useStyles };
