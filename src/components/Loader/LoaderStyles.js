import { makeStyles } from '@mui/styles';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    zIndex: '1000',
    minHeight: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    background: 'rgba(0,0,0,0.9)',
    backdropFilter: 'blur(4px)',
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center'
  },
  containerImage: {
    display: 'flex',
    justifyContent: 'center',
  },
  image: {
    transition: 'all 0.3s ease-in-out',
    animation: '$move-down 3s linear infinite',
  },
  '@keyframes move-down': {
    '0%': {
      '-webkit-transform': 'scale(0.8);transform:scale(0.8)'
    },
    '50%': {
      '-webkit-transform': 'scale(0.7);transform:scale(0.7)'
    },
    '100%': {
      '-webkit-transform': 'scale(0.8);transform:scale(0.8)'
    },
  },
  title: {
    fontSize: '60px !important'
  }
}));

export { useStyles };
