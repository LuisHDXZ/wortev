import React, { useState, useEffect } from 'react';
import { useStyles } from './LoaderStyles';
import { Grid, Paper, Typography } from '@mui/material';
import { images } from '../../assets/images';


export const Loader = () => {
  const classes = useStyles();
  const [imageIndex, setImageIndex] = useState(0);
  const { IronmanLogo, HulkLogo, ScarletLogo, SpidermanLogo, ThorLogo } = images
  const arrayImages = [
    HulkLogo, IronmanLogo, ScarletLogo, SpidermanLogo, ThorLogo
  ];


  useEffect(() => {
    const intervalId = setInterval(() => {
      setImageIndex(imageIndex => (imageIndex + 1) % arrayImages.length);
    }, 2000);
    return () => clearInterval(intervalId);
  });


  return (
    <Grid container className={classes.root}>
      <Grid item xs={12} className={classes.containerImage}>
        <img src={arrayImages[imageIndex]} alt={imageIndex} className={classes.image} />
      </Grid>
      <Grid item xs={12} className={classes.containerImage}>
        <Typography className={classes.title}>Cargando...</Typography>
      </Grid>
    </Grid>
  )
}
