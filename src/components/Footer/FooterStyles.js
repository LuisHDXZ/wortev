import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  mainfooter: {
    minHeight: '6vh',
    borderRadius: '5px 5px',
    boxShadow: '13px 10px 20px 8px rgba(255,0,0,0.78); - webkit - box - shadow: 13px 10px 20px 8px rgba(255,0,0,0.78); - moz - box - shadow: 13px 10px 20px 8px rgba(255,0,0,0.78);'
  },
  center: {
    display: 'flex',
    justifyItems: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: '6vh',
  },
  icon: {
    color: theme.palette.primary.main,
    transition: 'all 1s ease-in-out ',
    '& :hover': {
      color: 'red'
    }
  }
}));

export { useStyles };
