import React from 'react'
import { useStyles } from './FooterStyles';
import { Container, Grid, Typography } from '@mui/material';
import LinkedInIcon from '@mui/icons-material/LinkedIn';

export const Footer = () => {
  const classes = useStyles();

  return (
    <div className={classes.mainfooter}>
      <Container
        className={classes.container}

      >
        <Grid container  >
          <Grid item md={2} className={classes.center}>
            <Typography>Created By: Hernandez Luis</Typography>
          </Grid>
          <Grid item md={2} className={classes.center}>
            <a
              href='https://www.linkedin.com/in/luis-angel-hernandez-salas-99b5a325a'
              className={classes.icon}
              target='_blank'
            >
              <LinkedInIcon />
            </a>
          </Grid>
        </Grid>
      </Container>
    </div>
  )
}
