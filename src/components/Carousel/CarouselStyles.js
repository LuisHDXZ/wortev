import { makeStyles } from '@mui/styles';


const useStyles = makeStyles((theme) => ({
  link: {
    textDecoration: 'none',
    color: theme.palette.primary.main
  },
  centerContent: {
    display: 'flex',
    textAlign: 'center',
    alignSelf: 'center',
    transition: 'color 5.3s',
    '& :hover': {
      color: 'red'
    }
  }
}));

export { useStyles };
