import React from 'react'
import Carousel from "react-material-ui-carousel";
import { Grid } from '@mui/material';
import { ComicCard } from '../ComicCard/ComicCard';
import PropTypes from 'prop-types';

export const CarouselMedia = ({ items, updateImage }) => {

  const generateItems = (content) => {
    return content.map(content =>
      <Grid item md={4} xs={12}>
        <ComicCard heroName={content.title} description={`$ ${content.prices[0].price}`}
          thumbnail={content.thumbnail}
          updateImage={(image) => updateImage(image)}
        />
      </Grid>
    )
  }
  return (
    <Carousel autoPlay={true} stopAutoPlayOnHover={true} animation={'fade'}>
      {items.map((item) =>
        <Grid container spacing={3} style={{ width: '100%' }} >
          {generateItems(item)}
        </Grid>
      )
      }
    </Carousel >
  )
}

CarouselMedia.propTypes = {
  items: PropTypes.array.isRequired,
};