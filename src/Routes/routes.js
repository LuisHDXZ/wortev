import { Home } from "../views/Home/Home";
import { DetailHero } from "../views/DetailHero";
import { BaseLayout } from "../Layouts/BaseLayout";
import { Comics } from "../views/Comics";
import { HeroList } from "../views/HeroList";

const routes = [
  {
    path: '/',
    component: Home,
    layout: BaseLayout,
    exact: true
  },
  {
    path: '/Characteres',
    component: HeroList,
    layout: BaseLayout,
    exact: true
  },
  {
    path: '/Heroe',
    component: DetailHero,
    layout: BaseLayout,
    exact: true
  },
  {
    path: '/Comics',
    component: Comics,
    layout: BaseLayout,
    exact: true
  }
]

export { routes }