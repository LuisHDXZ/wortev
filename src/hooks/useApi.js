import { useState } from 'react';
import { instance } from '../api/instance'
import { changeLoader } from '../store/features/Loader/Loader';
import { useDispatch } from 'react-redux'

const useApi = ({ method, endpoint }) => {
  const dispatch = useDispatch()

  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const hiddenLoader = () => {
    setTimeout(() => {
      dispatch(changeLoader(false))
    }, 2000);
  }

  const fetchData = async (params) => {
    const compleUrl = () => {
      const ts = process.env.REACT_APP_MAIN_TS
      const apiKey = process.env.REACT_APP_MAIN_PUBLIC_KEY
      const has = process.env.REACT_APP_MAIN_HASH
      return `ts=${ts}&apikey=${apiKey}&hash=${has}`
    }
    const { body = {}, urlParams = null } = params
    const url = `${endpoint}?${compleUrl()}${urlParams ? `/${urlParams}` : ''}`;
    try {
      dispatch(changeLoader(true))
      const response = await instance[method](url, body);
      hiddenLoader()
      return response.data
    } catch (error) {
      setError(error);
    } finally {
      hiddenLoader()
    }
  };
  return [fetchData, data, error];
};

export { useApi };
