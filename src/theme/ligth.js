import { createTheme } from '@mui/material/styles';

const lightTheme = createTheme({
  typography: {
    fontFamily: ['Marvel']
  },
  palette: {
    mode: 'light',
    primary: {
      main: '#000',
      contrast: '#fff',
      contrastBg: '#000'
    },
    secondary: {
      main: '#ffff',
      contrast: '#308fe8'
    },
    background: {
      main: '#fff',
      secondary: '#EFEFEF',
    }
  },
});

export { lightTheme }