import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  mainContainer: {
    backgroundColor: theme.palette.background.main,
    color: theme.palette.primary.main
  },
  children: {
    minHeight: '89.2vh'
  }
}));

export { useStyles };
