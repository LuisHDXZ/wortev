import React from 'react'
import { Header } from '../../components/Header/Header'
import { Grid } from '@mui/material'
import { useStyles } from './BaseLayoutStyles';
import { Footer } from '../../components/Footer';
import { Loader } from '../../components/Loader';
import { useSelector } from 'react-redux'

export const BaseLayout = ({ children }) => {
  const classes = useStyles();
  const showLoader = useSelector((state) => state.loader.showLoader);

  return (
    <Grid container className={classes.mainContainer}>
      <div style={{ display: showLoader ? 'block' : 'none' }}>
        <Loader />
      </div>
      <Grid md={12} xs={12}>
        <Header />
      </Grid>
      <Grid md={12} xs={12} className={classes.children}>
        {children}
      </Grid>
      <Grid md={12} xs={12}>
        <Footer />
      </Grid>
    </Grid>
  )
}
