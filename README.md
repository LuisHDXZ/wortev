
## How to start the app

In order to start the application you need to install the necessary node modules for that we will execute:




```bash
  yarn
```
or 
```bash
  npm install
```


## create the .env file

To run this project, you will need to add the following environment variables to your .env file

`REACT_APP_MAIN_PRIVATE_KEY`

`REACT_APP_MAIN_PUBLIC_KEY`

`REACT_APP_MAIN_HASH`

`REACT_APP_MAIN_TS`

`REACT_APP_MAIN_BACKEND_URL`

## Demo

You can check the demo on

http://wortev-exam.s3-website-us-east-1.amazonaws.com/
